package com.example.xavier.ruleta;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button instruccions;
    private Button registre;
    private Button sortir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Boto de Instruccions
        instruccions = findViewById(R.id.Instruccions);
        instruccions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), Instruccions.class));
            }
        });

        // Boto de Registrar
        registre = findViewById(R.id.Registrar);
        registre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), Registrar.class));

            }
        });

        // Boto de sortida sense avisar
        sortir = findViewById(R.id.Sortir);
        sortir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();

            }
        });
    }

    //controlem el boto de tirar endarrere avisan
    @Override
    public void onBackPressed() {
        AlertDialog dialeg = new AlertDialog.Builder(this).setTitle("Sortir").setMessage("Desitges sortir?").setCancelable(false).setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finishAffinity();
            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show();
    }
}
