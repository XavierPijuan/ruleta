package com.example.xavier.ruleta;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Jugar extends AppCompatActivity {

    private TextView Ronda;
    private TextView Jugador1;
    private TextView Jugador2;
    private TextView Puntuacio1;
    private TextView Puntuacio2;
    private ImageView logo1;
    private ImageView logo2;
    private TextView Nom;
    private EditText Lletra;
    private Button pista;
    private Button validar;
    SharedPreferences preferences;

    private int r = 0;
    private int punt1 = 0;
    private int punt2 = 0;
    private String frase;
    private String pistaf;
    String[] Noms;// = getResources().getStringArray(R.array.Frases);
    String[] Pista;// = getResources().getStringArray(R.array.Pistes);
    char Paraula[];

    Random rnd = new Random();
    int numeroRnd = rnd.nextInt(4);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jugar);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        Noms = getResources().getStringArray(R.array.Frases);
        Pista = getResources().getStringArray(R.array.Pistes);

        Ronda = findViewById(R.id.Ronda);
        Puntuacio1 = findViewById(R.id.P1);
        Puntuacio2 = findViewById(R.id.P2);
        Nom = findViewById(R.id.Nom);
        pista = findViewById(R.id.Pista);
        validar = findViewById(R.id.Validar);
        Lletra = findViewById(R.id.lletra);
        Jugador1 = findViewById(R.id.J1);
        Jugador2 = findViewById(R.id.J2);
        logo1 = findViewById(R.id.logo1);
        logo2 = findViewById(R.id.logo2);

        frase = Noms[numeroRnd];
        pistaf = Pista[numeroRnd];

        Paraula = frase.toCharArray();
        for(int i = 0; i < frase.length(); i++){
            Paraula[i] = '_';
            /*if(Paraula[i] != ' '){
             Paraula[i] = '_';}*/
        }
        Nom.setText(new String(Paraula));


        r = r + 1;
        Ronda.setText(String.valueOf(r)+ "/10");
        //afegir condicio de ronda

        Jugador1.setText(getIntent().getStringExtra("NomJ1"));
        Jugador1.setTypeface(null, Typeface.BOLD);
        Jugador2.setText(getIntent().getStringExtra("NomJ2"));

        Puntuacio1.setText(String.valueOf(punt1)+" Punts");
        Puntuacio2.setText(String.valueOf(punt2)+" Punts");
        //afegir condicio de punts

        logo1.setImageResource(getIntent().getIntExtra("Img1", R.drawable.fairytail));
        logo2.setImageResource(getIntent().getIntExtra("Img2", R.drawable.fairytail));


        pista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog dialog = new AlertDialog.Builder(getBaseContext(),R.style.Theme_AppCompat)
                        .setTitle("Pista")
                        .setMessage(pistaf)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        }).show();
            }
        });

        validar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lletra = Lletra.getText().toString();

                if (lletra.isEmpty()) {
                    Lletra.setError("Introdueix lletra per a validar");
                }

            }
        });

    }
}
