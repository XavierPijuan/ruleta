package com.example.xavier.ruleta;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class Logos extends AppCompatActivity {

    ListView listView;
    String NomsFotos[] = {"Fairy Tail", "Hunter x Hunter", "Naruto", "One Piece"};
    int fotos[] = {R.drawable.fairytail, R.drawable.hunter, R.drawable.naruto, R.drawable.onepiece};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logos);
        listView = findViewById(R.id.simpleListView);
        CustomAdapter customAdapter = new CustomAdapter(this, NomsFotos, fotos);
        listView.setAdapter(customAdapter);

    }
}
