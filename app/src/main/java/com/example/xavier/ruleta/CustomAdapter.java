package com.example.xavier.ruleta;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CustomAdapter extends BaseAdapter {

    Context context;
    String NomsFotos[];
    int fotos[];
    LayoutInflater inflter;

    public CustomAdapter(Context applicationContext, String[] NomsFotos, int[] fotos) {
        this.context = applicationContext;
        this.NomsFotos = NomsFotos;
        this.fotos = fotos;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return NomsFotos.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.llista_fotos, null);
        TextView country = (TextView) view.findViewById(R.id.texto1);
        ImageView icon = (ImageView) view.findViewById(R.id.foto1);
        country.setText(NomsFotos[i]);
        icon.setImageResource(fotos[i]);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (i == 0) {
                    Toast.makeText(context, "Has triat Fairy Tail", Toast.LENGTH_SHORT).show();

                }
                if (i == 1) {
                    Toast.makeText(context, "Has triat Hunter X Hunter", Toast.LENGTH_SHORT).show();
                }
                if (i == 2) {
                    Toast.makeText(context, "Has triat Naruto", Toast.LENGTH_SHORT).show();
                }
                if (i == 3) {
                    Toast.makeText(context, "Has triat One Piece", Toast.LENGTH_SHORT).show();
                }
                ((Activity)context).setResult(i);
                ((Activity)context).finish();
            }
        });
        return view;
    }



}
