package com.example.xavier.ruleta;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class Registrar extends AppCompatActivity {

    private ImageButton icona1;
    private ImageButton icona2;
    private EditText jugador1;
    private EditText jugador2;
    private Button jugar;
    private int imatge1;
    private int imatge2;

    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        jugar = findViewById(R.id.Jugar);
        jugador1 = findViewById(R.id.J1);
        jugador2 = findViewById(R.id.J2);

        icona1 = findViewById(R.id.icona1);
        icona2 = findViewById(R.id.icona2);

        canviarImatge(preferences.getInt("Jugador1", 100), (preferences.getInt("Jugador2", 100)));

        icona1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getBaseContext(), Logos.class), 100);
            }
        });

        icona2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getBaseContext(), Logos.class), 101);
            }
        });

        jugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String j1 = jugador1.getText().toString();
                String j2 = jugador2.getText().toString();

                if (j1.isEmpty()) {
                    jugador1.setError("Introdueix el nom del Jugador 1");
                    if (j2.isEmpty()) {
                        jugador2.setError("Introdueix el nom del Jugador 2");
                    }
                } else if(j2.isEmpty()) {
                    jugador2.setError("Introdueix el nom del Jugador 2");
                }else if(j1.equals(j2)) {
                    jugador1.setError("El nom del Jugador 1 no pot ser igual al nom del Jugador 2");
                    jugador2.setError("El nom del Jugador 2 no pot ser igual al nom del Jugador 1");
                } else if(preferences.getInt("Jugador1",100) == 100){
                    Toast.makeText(Registrar.this, "Jugador 1 tria una imatge", Toast.LENGTH_SHORT).show();
                } else if(preferences.getInt("Jugador2",100) == 100){
                    Toast.makeText(Registrar.this, "Jugador 2 tria una imatge", Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(getBaseContext(), Jugar.class);
                    intent.putExtra("NomJ1", j1);
                    intent.putExtra("NomJ2", j2);
                    intent.putExtra("Img1", imatge1);
                    intent.putExtra("Img2", imatge2);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 100) {
            preferences.edit().putInt("Jugador1", resultCode).commit();

            if (resultCode == 0) {
                icona1.setImageResource(R.drawable.fairytail);
                imatge1 = R.drawable.fairytail;
            }
            if (resultCode == 1) {
                icona1.setImageResource(R.drawable.hunter);
                imatge1 = R.drawable.hunter;
            }
            if (resultCode == 2) {
                icona1.setImageResource(R.drawable.naruto);
                imatge1 = R.drawable.naruto;
            }
            if (resultCode == 3) {
                icona1.setImageResource(R.drawable.onepiece);
                imatge1 = R.drawable.onepiece;
            }
        }

        if (requestCode == 101) {
            preferences.edit().putInt("Jugador2", resultCode).commit();

            if (resultCode == 0) {
                icona2.setImageResource(R.drawable.fairytail);
                imatge2 = R.drawable.fairytail;
            }
            if (resultCode == 1) {
                icona2.setImageResource(R.drawable.hunter);
                imatge2 = R.drawable.hunter;
            }
            if (resultCode == 2) {
                icona2.setImageResource(R.drawable.naruto);
                imatge2 = R.drawable.naruto;
            }
            if (resultCode == 3) {
                icona2.setImageResource(R.drawable.onepiece);
                imatge2 = R.drawable.onepiece;
            }
        }
    }


    //controlem el boto de tirar endarrere(sense tancar l'aplicacio)
    @Override
    public void onBackPressed() {
        finish();
    }

    private void canviarImatge(int idImatge1, int idImatge2) {
        if (idImatge2 == 0) {
            icona2.setImageResource(R.drawable.fairytail);
            imatge2 = R.drawable.fairytail;
        }
        if (idImatge2 == 1) {
            icona2.setImageResource(R.drawable.hunter);
            imatge2 = R.drawable.hunter;
        }
        if (idImatge2 == 2) {
            icona2.setImageResource(R.drawable.naruto);
            imatge2 = R.drawable.naruto;
        }
        if (idImatge2 == 3) {
            icona2.setImageResource(R.drawable.onepiece);
            imatge2 = R.drawable.onepiece;
        }

        if (idImatge1 == 0) {
            icona1.setImageResource(R.drawable.fairytail);
            imatge1 = R.drawable.fairytail;
        }
        if (idImatge1 == 1) {
            icona1.setImageResource(R.drawable.hunter);
            imatge1 = R.drawable.hunter;
        }
        if (idImatge1 == 2) {
            icona1.setImageResource(R.drawable.naruto);
            imatge1 = R.drawable.naruto;
        }
        if (idImatge1 == 3) {
            icona1.setImageResource(R.drawable.onepiece);
            imatge1 = R.drawable.onepiece;
        }
    }
}